/* -------------------------------------------------------------------------------------------------------------- 
                            Drawing  objects to Canvase as per level 
-------------------------------------------------------------------------------------------------------------- */

function drawPlayer({ Player, ctx }) {
  ctx.fillStyle = Player.playerColour;
  ctx.fillRect(Player.x, Player.y, Player.width, Player.height);
}

/* -------------------------------------------------------------------------------------------------------------- 
                                Check Collisions of Player vs Other Objects
-------------------------------------------------------------------------------------------------------------- */

function collisionTrueFalse(r1,r2){
    return !(r1.x >= r2.x + r2.width 
            || r1.x + r1.width <= r2.x 
            || r1.y > r2.y + r2.height 
            || r1.y + r1.height <= r2.y);
}


//check collision by sides and not corners
function collisionSides(r1,r2) {
  var w = 0.5 * (r1.width + r2.width);
  var h = 0.5 * (r1.height + r2.height);
  var dx = (r1.x + r1.width/2) - (r2.x + r2.width/2);
  var dy = (r1.y + r1.height/2) - (r2.y + r2.height/2);
  var collision = 'none';

  if (Math.abs(dx) <= w && Math.abs(dy) <= h) {
      var wy = w * dy;
      var hx = h * dx;

      if (wy > hx) {
          if (wy >= -hx) {
            collision = "top";
          } else {
            collision = "left";
          }
      } else {
        if (wy >= -hx) {
          collision = "right";
        } else {
          collision = "bottom";
        }
      }
  }
  return collision;
}
  
  /* -------------------------------------------------------------------------------------------------------------- 
                                                       loader  
 -------------------------------------------------------------------------------------------------------------- */

 function loading(isLoading) {
  const loader = document.querySelector('.loader');
  if (isLoading) {
    loader.style.display = 'block';
  } else {
    loader.style.display = 'none';

  }
}


/* -------------------------------------------------------------------------------------------------------------- 
                                                       Text to Canvas Functions  
 -------------------------------------------------------------------------------------------------------------- */

function TextDraw(text, x, y, color, font_size, ctx){
  ctx.font = font_size + " Verdana";
  ctx.fillStyle = color || "#000";
  ctx.fillText(text, x, y);
}