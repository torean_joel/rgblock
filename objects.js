/* -------------------------------------------------------------------------------------------------------------- 
                                                       OBJECTS / functions
 -------------------------------------------------------------------------------------------------------------- */

 function Player() {
  this.width = 15;
  this.height = 15; // 15 default height
  this.x = canvasWidth/2 - 340;
  this.y = canvasHeight - 47;
  this.speed = 3;
  this.velX = 0;
  this.velY = 0;
  this.isUpKey = false;  
  this.isDownKey = false;
  this.isLeftKey = false;
  this.isRightKey = false;
  this.isPowerJump = false;
  this.jumping = false;
  this.playerColour = "#81CAFF";
}

function Platform(x,y,width,height,color, ctx) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y;
  this.colour = color;

  ctx.fillStyle = this.color;
  ctx.fillRect(x,y,width,height);
}

function PlatformNeutral(x,y,width,height, ctx) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y;
  this.colour = 'rgba(0,0,0,0)';

  ctx.fillStyle = this.colour;
  ctx.fillRect(x,y,width,height);
}

function Enemy(x,y,width,height, ctx) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y;

  ctx.fillStyle = 'rgba(0,0,0,0)';
  ctx.fillRect(x,y,width,height);
}

function Exit(x,y,width,height, ctx) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y;

  ctx.fillStyle = "#fff";
  ctx.fillRect(x,y,width,height);
}

//image of the background
function CreateImg(loc,x,y,w,h, ctx) {
  this.x = x;
  this.y = y;
  this.w = w;
  this.h = h;

  var img = new Image();
  img.src = loc;
  //background
  ctx.drawImage(img, x, y, w, h);
}
