/* -------------------------------------------------------------------------------------------------------------- 
                                      Canvas Setup and Variables
-------------------------------------------------------------------------------------------------------------- */

var canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d'),
    canvasWidth = canvas.width,
    canvasHeight = canvas.height;

var Player = new Player(),
    gravity = 0.3,
    friction = 0.80,
    Platforms = [],
    Ground = [],
    Enemies = [],
    Exits = [],
    Timer = 0;
    keys = {};

var level = 0,
    isPlaying = true; 

/* -------------------------------------------------------------------------------------------------------------- 
                            Start the loop when the Page is loaded
-------------------------------------------------------------------------------------------------------------- */
window.addEventListener("load", function(){
  //code executes once the page is loadedy
  TitleScreen();
});

/* -------------------------------------------------------------------------------------------------------------- 
                                Setup Animation for each Browser Prefix 
-------------------------------------------------------------------------------------------------------------- */

var requestAnimFrame =  window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.msRequestAnimationFrame  ||  
window.oRequestAnimationFrame   || 
function(callback) {
  window.setTimeout(callback, 1000/20);
};

/* -------------------------------------------------------------------------------------------------------------- 
                                Clear The console memory
-------------------------------------------------------------------------------------------------------------- */

/*var clearConsole = setInterval(function(){
  console.clear();
},1500);*/

/* -------------------------------------------------------------------------------------------------------------- 
                                            Game Loop Function 
-------------------------------------------------------------------------------------------------------------- */

function Loop() {
  
  if (isPlaying == true) {
    Platforms = [];
    Enemies = [];
    Exits = [];
    Ground = [];

    Prepare();

    // we need to check that the user has time or end the game
    if(level === 0) {
      Prepare();
    } else if (level === 1) {
      loading(true);
      Level1();
      drawPlayer({ Player, ctx });
      //Level Info
      new TextDraw("Level 1: Get to the exit", 20, 30, "#fff", "15px", ctx);
      loading(false);
    } else if (level === 2) {
      loading(true);
      Level2();
      drawPlayer({ Player, ctx });
      //Level Info
      new TextDraw("Level 2: You can slide up walls", 20, 30, "#fff", "15px", ctx);
      loading(false);
    } else if (level === 3) {
      loading(true);
      Level3();
      drawPlayer({ Player, ctx });
      //Level Info
      new TextDraw("Level 3: More walls more slides", 20, 30, "#fff", "15px", ctx);
      loading(false);
    } else if(level === 4) {
      loading(true);
      Level4();
      drawPlayer({ Player, ctx });

      //Level Info
      new TextDraw("Level 4: R: Red, G: Green, B: Blue, SPACE: Less Gravity", 20, 30, "#fff", "15px", ctx);
      loading(false);
    } else if(level === 5) {
      loading(true);
      Level5();
      drawPlayer({ Player, ctx });

      //Level Info
      new TextDraw("Level 5: Get to the exit", 20, 30, "#fff", "15px", ctx);
      loading(false);
    } else if (level === 6) {
      loading(true);
      Level6();
      drawPlayer({ Player, ctx });

      //Level Info
      new TextDraw("Level 6: Dont Touch Yellow blocks", 20, 30, "#fff", "15px", ctx);
      loading(false);
    } else {
      console.log("Game over - Torean Joel 2020");
      ctx.fillStyle = "#000";
      ctx.fillRect(0,0,canvasWidth,canvasHeight);
      isPlaying = false;
      TitleScreen();
    }

    movePlayer();  
    Collisions();

    requestAnimFrame(Loop);
  }
}

/* -------------------------------------------------------------------------------------------------------------- 
                          Prepare Function that will load before the scene is made 
-------------------------------------------------------------------------------------------------------------- */

function Prepare() {
  //also make menu before this
  ctx.fillStyle = "rgba(0,0,0,0)";
  ctx.fillRect(0,0,canvasWidth,canvasHeight);
}

function TitleScreen() {
  //drawing text to screen
  var isTitleScreen = true;
  var startedGame = false;
  var startScreen = document.getElementById('start');
  //default properties of markup
  startScreen.style.display = "block";
  controls.style.display = "none";
  canvas.style.display = "none";
  level = 1;
  isPlaying = true;

  document.body.addEventListener('keydown', function (e) {
    var keyID = e.code;

    if(keyID === "Enter" && isTitleScreen) {
      startedGame = true;
      isTitleScreen = false;
      startScreen.style.display = "none";
      canvas.style.display = "block";
      level = 1; //level 1
      // start the game loop after the user starts the game
      Loop();
    }
    if(keyID === "KeyH" && isTitleScreen && !startedGame) {
      isTitleScreen = false;
      startScreen.style.display = "none";
      controls.style.display = "block";
    }else {
      if(keyID === "KeyH" && !isTitleScreen && !startedGame) {
        isTitleScreen = true;
        startScreen.style.display = "block";
        controls.style.display = "none";
      }
    }
  });
}

/* -------------------------------------------------------------------------------------------------------------- 
                                    Create Level Layouts (x,y,width,height,color)
-------------------------------------------------------------------------------------------------------------- */

function Level1() {
  //background - image(loc,x,y,w,h)
  new CreateImg("images/levels/01.png",0,0,canvasWidth,canvasHeight, ctx);

  //ground
  var ground1 = new PlatformNeutral(0,canvasHeight - 26,canvasWidth,canvasHeight, ctx)
  Ground.push(ground1);

  //end Exit
  var Exit1 = new Exit(canvasWidth - (canvasWidth - 680),canvasHeight - 46,8,20, ctx);
  Exits.push(Exit1);

}

function Level2() {
  //background - image(loc,x,y,w,h)
  new CreateImg("images/levels/02.png",0,0,canvasWidth,canvasHeight, ctx);

  //ground
  var ground1 = new PlatformNeutral(0,canvasHeight - 26,canvasWidth,canvasHeight, ctx)
  Ground.push(ground1);

  var ground2 = new PlatformNeutral(canvasWidth - 217,canvasHeight - 92,217,130, ctx)
  Ground.push(ground2);

  //end Exit
  var Exit1 = new Exit(canvasWidth - (canvasWidth - 750),canvasHeight - 112,8,20, ctx);
  Exits.push(Exit1);
}

function Level3() {
  //background - image(loc,x,y,w,h)
  new CreateImg("images/levels/03.png",0,0,canvasWidth,canvasHeight, ctx);

  //ground
  var ground1 = new PlatformNeutral(0,canvasHeight - 26,canvasWidth,canvasHeight, ctx)
  Ground.push(ground1);
  
  var ground2 = new PlatformNeutral(canvasWidth - 217,canvasHeight - 111,217,130, ctx)
  Ground.push(ground2);

  var ground3 = new PlatformNeutral(canvasWidth - 87,canvasHeight - 221,87,112, ctx)
  Ground.push(ground3);

  var ground4 = new PlatformNeutral(canvasWidth - 173,canvasHeight - 221,87,68, ctx)
  Ground.push(ground4);

  //end Exit
  var Exit1 = new Exit(canvasWidth - (canvasWidth - 780),canvasHeight - 241,8,20, ctx);
  Exits.push(Exit1);
}

function Level4() {
  //background - image(loc,x,y,w,h)
  new CreateImg("images/levels/04.png",0,0,canvasWidth,canvasHeight, ctx);

  //ground
  var ground1 = new PlatformNeutral(0,canvasHeight - 26,184,canvasHeight, ctx)
  Ground.push(ground1);
  
  var ground2 = new PlatformNeutral(canvasWidth - 198,canvasHeight - 26, 198,canvasHeight, ctx)
  Ground.push(ground2);

  //platforms
  var plat1 = new Platform(canvasWidth - 335,canvasHeight - 89, 214,25, '#F00', ctx)
  Platforms.push(plat1);
  
  var plat2 = new Platform(131,canvasHeight - 89, 214,25, '#6FDE41', ctx)
  Platforms.push(plat2);

  //end Exit
  var Exit1 = new Exit(canvasWidth - (canvasWidth - 760),canvasHeight - 46,8,20, ctx);
  Exits.push(Exit1);
}

function Level5() {
  //background - image(loc,x,y,w,h)
  new CreateImg("images/levels/05.png",0,0,canvasWidth,canvasHeight, ctx);

  /* 1 -red 2-blue 3-blue 4-green 5-red */
  //ground
  var ground1 = new PlatformNeutral(0,canvasHeight - 26,canvasWidth,canvasHeight, ctx)
  Ground.push(ground1);

  var ground2 = new PlatformNeutral(0,canvasHeight - 171,64,150, ctx)
  Ground.push(ground2);

  //platforms
  var plat1 = new Platform(111,canvasHeight - 317,25,181, '#F00', ctx)
  Platforms.push(plat1);

  var plat2 = new Platform(186,canvasHeight - 347,177,25, '#81CAFF', ctx)
  Platforms.push(plat2);

  var plat3 = new Platform(canvasWidth - 342,canvasHeight - 291,50,50, '#81CAFF', ctx)
  Platforms.push(plat3);

  var plat4 = new Platform(canvasWidth - 250,canvasHeight - 362,50,50, '#6FDE41', ctx)
  Platforms.push(plat4);

  var plat5 = new Platform(canvasWidth - 169,canvasHeight - 402,169,25, '#F00', ctx)
  Platforms.push(plat5);

  //ground level
  var ground8 = new PlatformNeutral(canvasWidth - 272,canvasHeight - 89,272,63, ctx);
  Ground.push(ground8);

  var ground9 = new PlatformNeutral(canvasWidth - 179,canvasHeight - 168,179,79, ctx);
  Ground.push(ground9);

  var ground10 = new PlatformNeutral(canvasWidth - 75,canvasHeight - 215,75,47, ctx);
  Ground.push(ground10);

  //end Exit
  var Exit1 = new Exit(canvasWidth - (canvasWidth - 760),canvasHeight - 422,8,20, ctx);
  Exits.push(Exit1);
}

function Level6() {
  //background - image(loc,x,y,w,h)
  new CreateImg("images/levels/06.png",0,0,canvasWidth,canvasHeight, ctx);

  //ground
  var ground1 = new PlatformNeutral(0,canvasHeight - 26,299,canvasHeight, ctx)
  Ground.push(ground1);

  var ground2 = new PlatformNeutral(canvasWidth - 99,canvasHeight - 52,299,26, ctx)
  Ground.push(ground2);

  //platforms
  var plat1 = new Platform(318,canvasHeight - 85,122,25, '#81CAFF', ctx)
  Platforms.push(plat1);

  var plat2 = new Platform(canvasWidth - 318,canvasHeight - 161,74,25, '#6FDE41', ctx)
  Platforms.push(plat2);

  var plat4 = new Platform(canvasWidth - 223,canvasHeight - 268,25,268, '#81CAFF', ctx)
  Platforms.push(plat4);

  //enemies
  var enemy1 = new Enemy(canvasWidth - 173,canvasHeight - 171,30,30, ctx);
  Enemies.push(enemy1);

  var enemy2 = new Enemy(canvasWidth - 56,canvasHeight - 171,30,30, ctx);
  Enemies.push(enemy2);

  //end Exit
  var Exit1 = new Exit(canvasWidth - 10,canvasHeight - 72,8,20, ctx);
  Exits.push(Exit1);
}

function Collisions() {

  //player touching ground platforms
  for (var i = 0; i < Ground.length; i++) { 
    var thisGround = Ground[i];
    var touching = collisionSides(Player,thisGround);

    if(touching === "left"){ 
        Player.jumping = false;
        Player.velX = -1;
        Player.velY = 1;
    }
    if(touching === "right"){ 
        Player.jumping = false;
        Player.velX = 1;
        Player.velY = 1;
    }
    if(touching === "top"){ 
        Player.jumping = true;
        Player.velY = 1;
    }
    if(touching === "bottom"){ 
        Player.jumping = false;
        Player.velY = -0.5;
        Player.y = thisGround.y - Player.height;
    }
  }

  //player touching platforms
  for (var i = 0; i < Platforms.length; i++) { 
    var currentPlatform = Platforms[i];
    var touching = collisionSides(Player,currentPlatform);

    if(touching === "left"){ 
      if(Player.playerColour === currentPlatform.colour) {
        Player.jumping = false;
        Player.velX = -1;
        Player.velY = 0.5;
      }
    }
    if(touching === "right"){ 
      if(Player.playerColour === currentPlatform.colour) {
        Player.jumping = false;
        Player.velX = 1;
        Player.velY = 0.5;
      }
    }
    if(touching === "top"){ 
      if(Player.playerColour === currentPlatform.colour) {
        Player.jumping = true;
        Player.velY = 1;
      }
    }
    if(touching === "bottom"){ 
      if(Player.playerColour === currentPlatform.colour) {
        Player.jumping = false;
        Player.velY = 0;
        Player.y = currentPlatform.y - Player.height;
      }
    }
  }

  //player touching Enemy Object
  for (var i = 0; i < Enemies.length; i++) { 
    var currentEnemy = Enemies[i];
    var touching = collisionTrueFalse(currentEnemy,Player);

    if(touching) { 
      Player.jumping = false;
      Player.velY = 0;
      Player.velX = 0;
      Player.x = canvasWidth/2 - 340;
      Player.y = canvasHeight - 47;
    }
  }

  //player touching level objective and resetting arrays and moving to next level
  for (var i=0;i<Exits.length;i++) { 
    var currentExit = Exits[i];
    var touching = collisionTrueFalse(currentExit,Player);

    if(touching) { 
      Player.jumping = false;
      Player.velY = 0;
      Player.velX = 0;
      Player.x = canvasWidth/2 - 340;
      Player.y = canvasHeight - 47;

      Ground = [];
      Platforms = [];
      Exits = [];
      Walls = [];      

      level++;
    }
  }
}

/* -------------------------------------------------------------------------------------------------------------- 
                                Moving The Player Object
-------------------------------------------------------------------------------------------------------------- */

function movePlayer() {

  //checking the keys
  keyChecks();

  //moving the player
  if(!(Player.isLeftKey) && !(Player.isRightKey)) {
    if (Player.velX > 0) {
      Player.velX *= friction;
    }
    if (Player.velX < 0) {
      Player.velX *= friction;
    }
  }

  if(Player.isLeftKey) {
    if (Player.velX > -Player.speed){
      Player.velX--;
    }
  } else if(Player.isRightKey) {
    if (Player.velX < Player.speed) {   
      Player.velX++;
    }    
  }

  if(Player.isUpKey) {
    if(!Player.jumping) {
      Player.jumping = true;
      Player.velY = -Player.speed*2;
    }
  }

  Player.velY += gravity;
  Player.x += Player.velX;
  Player.y += Player.velY;
  //Prevent player from dropping and then still being able to air jump
  Player.jumping = true; //Only jump from a standing position

  if (Player.x >= canvasWidth - Player.width)  {
    Player.x = canvasWidth - Player.width;
  } else if (Player.x <= 0) {  
    Player.x = 0;
  }   

  if(Player.y >= canvasHeight-Player.height) {
    /*Player.y = canvasHeight - Player.height;
    Player.jumping = false;*/
    Player.jumping = false;
    Player.velY = 0;
    Player.velX = 0;
    Player.x = canvasWidth/2 - 340;
    Player.y = canvasHeight - 47;
  }
}

/* -------------------------------------------------------------------------------------------------------------- 
                                        Event Listeners - Key Bindings
-------------------------------------------------------------------------------------------------------------- */

document.body.addEventListener('keydown', (e) => {
  keys[e.code] = true; 
}, false);

//check if key is not being pressed or has lifted up
document.body.addEventListener('keyup', (e) => {
  delete keys[e.code];
}, false);

function keyChecks() {
  //checking keys pressed
  if (keys["KeyR"]) {
    Player.playerColour = '#F00'; //red
    Player.hue = 355;
    Player.saturation = 77;
    Player.lightness = 52;
  }
  if (keys["KeyG"]) {
    Player.playerColour = '#6FDE41'; //green
    Player.hue = 138;
    Player.saturation = 97;
    Player.lightness = 38;
  }
  if (keys["KeyB"]) {
    Player.playerColour = '#81CAFF'; //blue
    Player.hue = 210;
    Player.saturation = 79;
    Player.lightness = 55;
  }
  (keys["ArrowUp"]) ? Player.isUpKey = true : Player.isUpKey = false;
  (keys["ArrowRight"]) ? Player.isRightKey = true : Player.isRightKey = false;
  (keys["ArrowLeft"]) ? Player.isLeftKey = true : Player.isLeftKey = false;
  (keys["Space"]) ? Player.isPowerJump = true : Player.isPowerJump = false;

  (Player.isPowerJump) ? gravity = 0.25 : gravity = 0.4;
}